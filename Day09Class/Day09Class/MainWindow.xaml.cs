﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day09Class
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<string> FriendsList = new List<string>();
        public MainWindow()
        {
            InitializeComponent();
            lvFriends.ItemsSource = FriendsList;
        }

        private void miEditAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dlg = new AddEditDialog();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                string name = dlg.NameResult;
                FriendsList.Add(name);
                lvFriends.Items.Refresh();
            }
        }

        private void lvFriends_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            int index = lvFriends.SelectedIndex;
            if (index == -1) return;
            lvFriends.SelectedItem = index;
        }
    }
}
