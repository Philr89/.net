﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


namespace Day08Todo
{
    class Tasks
    {
        private string _task;
        private int _difficulty;
        private DateTime _dueDate;
        enum Status
        {
            Pending,
            Done,
            Delegated
        }

        public string Task
        {
            get { return _task; }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9 \.]{1,100}$"))
                {
                    MessageBox.Show("Task must be between 1-100 characters");
                }

                _task = Task;
            }
        }

    }
}
