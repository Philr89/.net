﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Day06AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = "";
            if (tbName.Text == "")
            {
                MessageBox.Show("Please enter a name");
            }
            else
            {
                name = tbName.Text;
            }
            string age = "";
            string pets = "";
            string continent;
            int preferredTemp;

            if (rbBelow18.IsChecked == true)
            {
                age = "Below 18";
            }
            else if (rb18To35.IsChecked == true)
            {
                age = "18 to 35";
            }
            else if (rb36AndUp.IsChecked == true)
            {
                age = "36 and up";
            }
            else
            {
                MessageBox.Show("No radio button selected");
                return;
            }

            if (ckCat.IsChecked == true)
            {
                if (pets == "")
                {
                    pets = "Cat";
                }
                else
                {
                    pets += ",Cat";
                }
            }

            if (ckDog.IsChecked == true)
            {
                if (pets == "")
                {
                    pets = "Dog";
                }
                else
                {
                    pets += ",Dog";
                }
            }

            if (ckOther.IsChecked == true)
            {
                if (pets == "")
                {
                    pets = "Other";
                }
                else
                {
                    pets += ",Other";
                }
            }

            if (cbContinent.Text == "Please choose...")
            {
                if (!(name == ""))
                    MessageBox.Show("Please select a continent");
                return;
            }
            else
            {
                continent = cbContinent.Text;
            }
            double temp = sliderTemp.Value;
            int tempInC = (int)temp;
            preferredTemp = tempInC;

            string path = @"../../AllInputs.txt";
            if (!File.Exists(path))
            {
                StreamWriter sw = File.CreateText(path);
            }

            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine($"{name};{age};{pets};{continent};{preferredTemp}C;");
            }
            MessageBox.Show("Successfully Registered");
            //Reset inputs
            tbName.Text = "";
            rbBelow18.IsChecked = true;
            ckCat.IsChecked = false;
            ckDog.IsChecked = false;
            ckOther.IsChecked = false;
            cbContinent.SelectedItem = "Please choose...";
            sliderTemp.Value = 25;
        }

        private void ComboBoxItem_Selected(object sender, RoutedEventArgs e)
        {

        }

        private void sliderTemp_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double temp = sliderTemp.Value;
            int tempInC = (int)temp;
            lblTemp.Content = $"Preferred temp {tempInC}C";
        }
    }
}
