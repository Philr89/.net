﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Person
    {
        private string _name;
        private int _age;

        public string Name
        {
            get { return _name; }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    Program.LoggerDelegator?.Invoke("Invalid person error");
                    throw new InvalidParameterException("Name must be 1-50 characters and cannot contain a semicolon");
                }
                else
                {
                    _name = value;
                }
            }
        }

        public int Age
        {
            get { return _age; }
            set
            {
                if (value < 0 || value > 150)
                {
                    Program.LoggerDelegator?.Invoke("Invalid age error");
                    throw new InvalidParameterException("Age must be between 0-150");
                }
                else
                {
                    _age = value;
                }
            }
        }
        //Constructors
        protected Person()
        {

        }
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public Person(string dataLine)
        {
            string[] dataSplit = dataLine.Split(';');
            if (dataSplit.Length != 3)
            {
                Program.LoggerDelegator?.Invoke("Person data line must have 3 fields");
                throw new InvalidParameterException("Invalid data");
            }
            if(dataSplit[0] != "Person")
            {
                Program.LoggerDelegator?.Invoke("This is not a person");
                throw new InvalidParameterException("Not a person");
            }
            Name = dataSplit[1];
            if (!int.TryParse(dataSplit[2], out int age))
            {
                Program.LoggerDelegator?.Invoke("Age must be an integer");
                throw new InvalidParameterException("Invalid number of items in line");
            }
            Age = age;
        }

        public override string ToString()
        {
            return $"Person {Name} is {Age} y/o";
        }

        public virtual string ToDataString()
        {
            return $"Person;{Name};{Age}";
        }
    }
}
