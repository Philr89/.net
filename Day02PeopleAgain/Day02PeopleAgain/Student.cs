﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Student : Person
    {
        private string _program;
        private double _gpa;

        public string Program
        {
            get { return _program; }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    throw new InvalidParameterException("Program must be between 1 and 50 characters and cannot contain a semicolon");
                }
                else
                {
                    _program = value;
                }
            }
        }

        public double GPA
        {
            get { return _gpa; }
            set
            {
                if (value < 0 || value > 4.3)
                {
                    throw new InvalidParameterException("GPA value must be between 0 and 4.3");
                }
                else
                {
                    _gpa = value;
                }
            }
        }

        public Student(string name, int age, string program, double gpa) : base(name, age)
        {
            Program = program;
            GPA = gpa;
        }

        //TODO: create dataline
        public Student(string dataLine)
        {
            string[] dataSplit = dataLine.Split(';');
            if (dataSplit.Length != 5)
            {
                throw new InvalidParameterException("Invalid data");
            }
            Name = dataSplit[1];
            if (!int.TryParse(dataSplit[2], out int age))
            {
                throw new InvalidParameterException("Invalid data");
            }
            Age = age;
            Program = dataSplit[3];
            if (!double.TryParse(dataSplit[4], out double gpa))
            {
                throw new InvalidParameterException("Invalid data");
            }
            GPA = gpa;
        }

        public override string ToString()
        {
            return $"Student {Name} is {Age} y/o and has a GPA of {GPA} in {Program}";
        }
        public override string ToDataString()
        {
            return $"Student;{Name};{Age};{Program};{GPA}";
        }
    }
}
