﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class Teacher : Person
    {
        private string _subject;
        private int _yearsOfExperience;

        public string Subject
        {
            get { return _subject; }
            set
            {
                if (value.Length < 1 || value.Length > 50 || value.Contains(";"))
                {
                    throw new InvalidParameterException("Subject must be 1-50 characters and cannot contain a semicolon");
                }
                else
                {
                    _subject = value;
                }
            }
        }

        public int YearsOfExperience
        {
            get { return _yearsOfExperience; }
            set
            {
                if (value < 0 || value > 100)
                {
                    throw new InvalidParameterException("Years must be between 0-100");
                }
                else
                {
                    _yearsOfExperience = value;
                }
            }
        }

        public Teacher(string name, int age, string subject, int yearsOfExperience) : base(name, age)
        {
            Subject = subject;
            YearsOfExperience = yearsOfExperience;
        }

        public Teacher(string dataLine)
        {
            string[] dataSplit = dataLine.Split(';');
            if (dataSplit.Length != 5)
            {
                throw new InvalidParameterException("Invalid data");
            }
            Name = dataSplit[1];
            if (!int.TryParse(dataSplit[2], out int age))
            {
                throw new InvalidParameterException("BAD DATA");
            }
            Age = age;
            Subject = dataSplit[3];
            if (!int.TryParse(dataSplit[4], out int yearsOfExperience))
            {
                throw new InvalidParameterException("Bad data");
            }
            YearsOfExperience = yearsOfExperience;
        }
        public override string ToString()
        {
            return $"Teacher {Name} is {Age} y/o and teacher {Subject} for {YearsOfExperience}";
        }

        public override string ToDataString()
        {
            return $"Teacher;{Name};{Age};{Subject};{YearsOfExperience}";
        }
    }
}
