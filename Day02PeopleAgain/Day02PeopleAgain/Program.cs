﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Day02PeopleAgain
{
    class Program
    {
        static List<Person> PeopleList = new List<Person>();
        const string DataFileName = @"..\..\people.txt";
        static double averageGpa;
        static int studentCount;

        public delegate void LogFailedSetterDelegate(string reason);

        public static LogFailedSetterDelegate LoggerDelegator;

        static public void LogErrorsToScreen(string msg)
        {
            Console.WriteLine("*** LOG: " + msg);
        }

        static public void LogErrorsToFile(string msg)
        {
            try
            {
                File.AppendAllText(@"..\..\log.txt", msg + "\n");
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                Console.WriteLine("Error saving log message to file: " + ex.Message);
            }
        }

        static public void ReadDataFromFile()
        {
            try
            {
                if (!File.Exists(DataFileName))
                {
                    return;
                }
                string[] readText = File.ReadAllLines(DataFileName); //IOException and SystemException
                foreach (String line in readText)
                {
                    string type = line.Split(';')[0];
                    try
                    {
                        switch (type)
                        {
                            case "Person":
                                Person person = new Person(line);
                                PeopleList.Add(person);
                                break;
                            case "Teacher":
                                Teacher teacher = new Teacher(line);
                                PeopleList.Add(teacher);
                                break;
                            case "Student":
                                Student student = new Student(line);
                                PeopleList.Add(student);
                                break;
                            default:
                                break;
                        }
                    }
                    catch (InvalidParameterException ex) when (ex is FormatException || ex is ArgumentException)
                    {
                        Console.WriteLine($"Error: {ex.Message} in:\n {line}");
                    }
                }
            }
            catch (InvalidParameterException ex)
            {
                Console.WriteLine("Error reading file: " + ex.Message);
            }

        }

        public static void LogErrors()
        {
            //Delegate
            int choice = 0;
            Console.WriteLine("Where would you like to log setters errors?\n" +
                "1-screen only\n" +
                "2-screen and file\n" +
                "3-do not log\n" +
                "Your choice: " + choice);

            Int32.TryParse(Console.ReadLine(), out int userChoice);
            choice = userChoice;

            if (choice >= 1 || choice <= 3)
            {
                switch (choice)
                {
                    case 1:
                        LoggerDelegator = LogErrorsToScreen;
                        break;
                    case 2:
                        LoggerDelegator += LogErrorsToFile;
                        break;
                    case 3:
                        Console.WriteLine("No errors logged");
                        break;
                    default:
                        break;
                }
            }
        }

        static void Main(string[] args)
        {
            LogErrors();
            ReadDataFromFile();

            foreach (Person person in PeopleList)
            {
                Console.WriteLine(person.ToString());
            }

            // LOOP of only Students
            Console.WriteLine("List of Students only ---------------------------------- ");
            foreach (Person student in PeopleList)
            {
                if (student is Student)
                {
                    Console.WriteLine(student.ToString());
                }
            }

            //LOOP for only Teachers
            Console.WriteLine("List of Teachers only ---------------------------------- ");
            foreach (Person dude in PeopleList)
            {
                if (dude is Teacher)
                {
                    Console.WriteLine(dude.ToString());
                }
            }

            //LOOP for only People
            Console.WriteLine("List of Persons only ---------------------------------- ");
            foreach (Person person in PeopleList)
            {
                if (person.GetType() == typeof(Person))
                {
                    Console.WriteLine(person.ToString());
                }
            }

            //Average student GPA

            foreach (Person student in PeopleList)
            {

                if (student is Student)
                {
                    Student castStudent = (Student)student;
                    averageGpa += castStudent.GPA;
                    studentCount++;
                }
            }
            averageGpa = averageGpa / studentCount;
            Console.WriteLine($"The average student GPA is {averageGpa}");

            //Median student GPA

            //LINQ


            Console.ReadKey();
        }
    }
}
