﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    public class Person
    {
        [Key]
        public int Id { get; set; }

        [Required] //Not null
        [StringLength(50)] //nvarchar(50)
        public string Name { get; set; }

        [Required]
        [Index] //Not unique, speeds up lookup
        public int Age { get; set; }

        /*
        [NotMapped] //In memory only, not in database
        public string Comment { get; set; }

        [EnumDataType(typeof(GenderEnum))]
        public GenderEnum Gender { get; set; }
        public enum GenderEnum { Male = 1, Female = 2, NA = 3 }
        */


    }
}
