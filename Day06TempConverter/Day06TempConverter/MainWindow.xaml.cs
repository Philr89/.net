﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06TempConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void convert()
        {
            double.TryParse(tbInput.Text, out double input);
            double output;
            string degrees = System.Net.WebUtility.HtmlDecode("&#176;");

            //Celcius conversions
            if (rbInputCelcius.IsChecked == true && rbOutputCelcius.IsChecked == true)
            {
                lblConvertedTemp.Content = input + degrees + "C";
            }
            else if (rbInputCelcius.IsChecked == true && rbOutputFahrenheit.IsChecked == true)
            {
                output = (input * 9 / 5) + 32;
                lblConvertedTemp.Content = output + degrees + "F";
            }
            else if (rbInputCelcius.IsChecked == true && rbOutputKelvin.IsChecked == true)
            {
                output = (input + 273.15);
                lblConvertedTemp.Content = output + "K";
            }

            //Fahrenheit conversion
            if (rbInputFahrenheit.IsChecked == true && rbOutputCelcius.IsChecked == true)
            {
                output = (input - 32) * 5 / 9;
                lblConvertedTemp.Content = output + degrees + "C";
            }
            else if (rbInputFahrenheit.IsChecked == true && rbOutputFahrenheit.IsChecked == true)
            {
                lblConvertedTemp.Content = input + degrees + "F";
            }
            else if (rbInputFahrenheit.IsChecked == true && rbOutputKelvin.IsChecked == true)
            {
                output = (input - 32) * 5 / 9 + 273.15;
                lblConvertedTemp.Content = output + "K";
            }

            //Kelvin conversion
            if (rbInputKelvin.IsChecked == true && rbOutputCelcius.IsChecked == true)
            {
                output = input - 273.15;
                lblConvertedTemp.Content = output + degrees + "C";
            }
            else if (rbInputKelvin.IsChecked == true && rbOutputFahrenheit.IsChecked == true)
            {
                output = (input - 273.15) * 9 / 5 + 32;
                lblConvertedTemp.Content = output + degrees + "F";
            }
            else if (rbInputKelvin.IsChecked == true && rbOutputKelvin.IsChecked == true)
            {
                lblConvertedTemp.Content = input + "K";
            }
        }

        private void rbOutputKelvin_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rbOutputFahrenheit_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rbOutputCelcius_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rbInputKelvin_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rbInputFahrenheit_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void rbInputCelcius_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void tbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            //Figure out why it crashed if you call convert() here
        }

        private void rbInputCelcius_Click(object sender, RoutedEventArgs e)
        {
            convert();
        }

        private void rbInputFahrenheit_Click(object sender, RoutedEventArgs e)
        {
            convert();
        }

        private void rbInputKelvin_Click(object sender, RoutedEventArgs e)
        {
            convert();
        }

        private void rbOutputCelcius_Click(object sender, RoutedEventArgs e)
        {
            convert();
        }

        private void rbOutputFahrenheit_Click(object sender, RoutedEventArgs e)
        {
            convert();
        }

        private void rbOutputKelvin_Click(object sender, RoutedEventArgs e)
        {
            convert();
        }

        private void tbInput_KeyUp(object sender, KeyEventArgs e)
        {
            convert();
        }
    }
}
