﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01People
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string[] namesArray = new string[4];
                int[] agesArray = new int[4];
                for (int i = 0; i < 4; i++)
                {
                    Console.WriteLine("Enter name #{0}:", i + 1);
                    string name = Console.ReadLine();
                    namesArray[i] = name;

                    Console.Write("Enter age #{0}: ", i + 1);
                    string ageStr = Console.ReadLine();
                    //parsing using output parameter
                    int age;
                    if (int.TryParse(ageStr, out age) == false);
                    {
                        Console.WriteLine("Invalid input. Exiting");
                        return;
                    }
                }
                    //old fashioned way

                    /*
                        try
                        {
                            int age = int.Parse(ageStr);
                            agesArray[i] = age;
                        } catch (FormatException ex)
                        {

                        }
                    */
                
                // print them back:
                Console.WriteLine("You entered: ");
                for (int i = 0; i < 4; i++)
                {
                    Console.Write("{0}{1}", i == 0 ? "" : ", ", namesArray[i]);
                }

            }
            finally
            {
                Console.Write("\nPress any key to end");
                Console.ReadKey();
            }
        }

    }
}
