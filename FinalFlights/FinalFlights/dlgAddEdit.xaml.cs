﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.IO;

namespace FinalFlights
{
    /// <summary>
    /// Interaction logic for dlgAddEdit.xaml
    /// </summary>
    public partial class dlgAddEdit : Window
    {
        public dlgAddEdit()
        {
            InitializeComponent();
        }

        private DateTime _date;
        public DateTime Date
        {
            get { return _date; }
        }

        private string _fromCode;
        public string FromCode
        {
            get { return _fromCode; }
        }

        private string _toCode;
        public string ToCode
        {
            get { return _fromCode; }
        }

        private EType _type;
        public EType Type
        {
            get { return _type; }
        }

        private int _passengers;
        public int Passengers
        {
            get { return _passengers; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void sliderPassengers_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        public void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //Date
            if (dpDate.SelectedDate == null)
            {
                MessageBox.Show("Please select a date");
                return;
            }
            DateTime Date = dpDate.SelectedDate.Value.Date;
            _date = Date;

            //From Code
            if (!Regex.IsMatch(tbFromCode.Text, @"^[a-zA-Z0-9]{3,5}$"))
            {
                MessageBox.Show("From code must be 3-5 characters");
                return;
            }
            string FromCode = tbFromCode.Text;
            _fromCode = FromCode;

            //To Code
            if (!Regex.IsMatch(tbToCode.Text, @"^[a-zA-Z0-9]{3,5}$"))
            {
                MessageBox.Show("To code must be 3-5 characters");
                return;
            }
            string ToCode = tbToCode.Text;
            _toCode = ToCode;

            //Type
            EType Type = EType.International;

            if (cbType.Text == "Please choose...")
            {
                MessageBox.Show("Please select a flight type");
                return;
            }
            else if (cbType.Text == "Domestic")
            {
                Type = EType.Domestic;
                _type = Type;
            }
            else if (cbType.Text == "International")
            {
                Type = EType.International;
                _type = Type;
            }
            else if (cbType.Text == "Private")
            {
                Type = EType.Private;
                _type = Type;
            }

            //Passengers
            int Passengers = Convert.ToInt32(sliderPassengers.Value);

            Flights flight = new Flights(Date, FromCode, ToCode, Type, Passengers);

            ((MainWindow)Application.Current.MainWindow).lvFlights.Items.Refresh();
            this.Close();
        }
    }
}
