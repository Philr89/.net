﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinalFlights
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string doc = "@../../flights.txt";
        List<Flights> flightList = new List<Flights>();


        public MainWindow()
        {
            InitializeComponent();
            lvFlights.ItemsSource = flightList;
            LoadDataFromFile();
        }

        private void LoadDataFromFile()
        {
            Flights flightsInstance = new Flights();
            try
            {
                if (!File.Exists(doc)) return;
                string[] linesList = File.ReadAllLines(doc);
                foreach (string line in linesList)
                {
                    try
                    {
                        Flights flight = new Flights(line);
                        flightList.Add(flight);
                    }
                    catch (InvalidDataException ex)
                    {
                        MessageBox.Show(this, "Error reading from file: " + ex.Message);
                    }
                    File.WriteAllLines(doc, linesList);
                }
            }
            catch (IOException ex)
            {
                throw new InvalidDataException("Error reading from file: " + ex.Message);
            }
        }
        private void SaveDataToFile(string doc, List<Flights> flightsToSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Flights flight in flightsToSave)
                {
                    linesList.Add(flight.ToDataString());
                }
                File.WriteAllLines(doc, linesList);
            }
            catch (IOException ex)
            {
                throw new InvalidDataException("Error saving to file: " + ex.Message);
            }

        }

        private void MenuItem_AddClick(object sender, RoutedEventArgs e)
        {
            dlgAddEdit dlg = new dlgAddEdit();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                DateTime Date = dlg.Date;
                string FromCode = dlg.FromCode;
                string ToCode = dlg.ToCode;
                EType Type = dlg.Type;
                int Passengers = dlg.Passengers;

                Flights flight = new Flights(Date, FromCode, ToCode, Type, Passengers);
                flightList.Add(flight);
                lvFlights.Items.Refresh();
            }
            SaveDataToFile(doc, flightList);
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Flights flightsInstance = new Flights();
            SaveDataToFile(doc, flightList);
        }
    }
}

