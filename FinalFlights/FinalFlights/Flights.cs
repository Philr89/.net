﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Globalization;

namespace FinalFlights
{
    public enum EType
    {
        Domestic,
        International,
        Private
    }
    class Flights
    {
        private DateTime _date;
        private string _fromCode;
        private string _toCode;
        private EType _type;
        private int _passengers;

        public Flights(DateTime date, string fromCode, string toCode, EType type, int passengers)
        {
            Date = date;
            FromCode = fromCode;
            ToCode = toCode;
            Type = type;
            Passengers = passengers;
        }

        public Flights(string dataLine)
        {
            try
            {
                string[] data = dataLine.Split(';');
                if (data.Length != 5)
                {
                    throw new InvalidDataException("Invalid data from file");
                }
                Date = DateTime.ParseExact(data[0], "yyyy-MM-dd", CultureInfo.InvariantCulture);
                FromCode = data[1];
                ToCode = data[2];

                try
                {
                    Type = (EType)Enum.Parse(typeof(EType), data[3], true);
                }
                catch (ArgumentException ex)
                {
                    throw new InvalidDataException("Invalid Type: " + ex.Message);
                }

                Passengers = Int32.Parse(data[4]);
            }
            catch (FormatException ex)
            {
                throw new InvalidDataException("Data format invalid in line: " + ex.Message);
            }
        }

        public Flights()
        {

        }
        public DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public string FromCode
        {
            get { return _fromCode; }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9]{3,5}$"))
                {
                    throw new InvalidDataException("Code must be between 3-5 characters or digits");
                }
                _fromCode = value;
            }
        }

        public string ToCode
        {
            get { return _toCode; }
            set
            {
                if (!Regex.IsMatch(value, @"^[a-zA-Z0-9]{3,5}$"))
                {
                    throw new InvalidDataException("Code must be between 3-5 characters or digits");
                }
                _toCode = value;
            }
        }

        public EType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public int Passengers
        {
            get { return _passengers; }
            set { _passengers = value; }
        }

        public string ToDataString()
        {
            return $"{Date};{FromCode};{ToCode};{Type};{Passengers}";
        }
    }
}
